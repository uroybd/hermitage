---
{"title":"The Sea","created":"2022-10-06T00:00:00+06:00","updated":"2023-01-03T21:19:24+06:00","read_at":["2022-10-08T00:00:00+06:00"],"read_count":1,"authors":["John Banville"],"isbn10":1400097029,"status":"Read","rating":5,"reviewed":true,"cover":"http://books.google.com/books/content?id=ovu5y7K6eg8C&printsec=frontcover&img=1&zoom=1&source=gbs_api","dg-publish":true,"permalink":"/personal/reading/books/read/the-sea-by-john-banville/","dgPassFrontmatter":true}
---

### Review
Every book doesn't impress on the same place although we take it for granted that they impress upon our feelings. The impression of a book on me is often physical. Some books make my brain fuzzy, some are like deep wounds in my groin, and some are like empty little pockets in my chest. The Sea took my body and broke every bone of it, pulled every muscle.

Since it is a novel, the first question you may ask is, "What is the story?" Well, it doesn't matter. Stories happen all the time and every story is as old as time. Who, and most importantly, how the story is being told is what modern literature is concerned about. When Max, the story-teller here, a self-made man, often vain, mostly sensitive, an intellectual born and brought up in the lower segments of the society tells his story candidly, yet always trying to justify his actions, losses and laughs re-interpreted with the hindsight of a well-aged man, but not entirely coherent— that is not a story anymore. That is life. And life in its most raw form cannot be judged but only be accepted. This acceptance comes with a deep complex sensation, not love, not pity, not hope definitely, but like you're drowning, in the sea.

Notes:: [[Personal/Reading/Notes and Highlights/The Sea by John Banville\|The Sea by John Banville]]

### About The Sea by John Banville
| <!-- -->    | <!-- -->    |
|-------------|-------------|
| ![The Sea\|150](http://books.google.com/books/content?id=ovu5y7K6eg8C&printsec=frontcover&img=1&zoom=1&source=gbs_api)         | When art historian Max Morden returns to the seaside village where he once spent a childhood holiday, he is both escaping from a recent loss and confronting a distant trauma. The Grace twins, Myles and Chloe, fascinated Max. He grew to know them intricately, and what ensued would haunt him for the rest of his years.         |

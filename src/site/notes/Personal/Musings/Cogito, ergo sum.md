---
{"title":"Cogito, ergo sum","updated":"2023-01-03T20:39:06+06:00","tags":["philosophy"],"created":"2022-05-02T20:36:00+06:00","location":"Badda, Dhaka","dg-publish":true,"permalink":"/personal/musings/cogito-ergo-sum/","dgPassFrontmatter":true}
---

# Cogito, ergo sum

Practically all modern self-searching starts with "Cogito, ergo sum", i.e. "I think, therefore I am."

I think, I perceive, and I see the world with utmost subjectivity. I find myself, quite naturally, in the centre of my world. My historicity defines the history of my world. This **I** became so valuable I cannot imagine a world without it. Indeed, I **MUST** be, be it in the afterlife, an endless cycle of rebirth, or simply as a tiny flux of consciousness stored in some corner of the universe.

Or, I think, therefore like every thinking things I exist. I share an objective world and history with many other things. This **I** is as much significant as others of its kind. Like them, it can arise from and dissolve into unconsciousness. This doesn't allow an eternal soul but can bestow one with a full moon over a pine forest with an 'ah!' barely audible, a sunrise that set our whole life ablaze, in short, the [[Entities/Concept/Mono no aware\|pathos of things]].
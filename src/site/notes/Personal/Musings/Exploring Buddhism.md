---
{"title":"Exploring Buddhism","updated":"2023-01-03T21:47:48+06:00","tags":["buddhism","philosophy"],"created":"2021-08-30T20:24:08+06:00","dg-publish":true,"permalink":"/personal/musings/exploring-buddhism/","dgPassFrontmatter":true}
---

# Exploring Buddhism

For quite some time, I'm exploring Buddhism. I've started with ’**What Buddha Taught**’, have learned some more from a book on the history and development of Buddhism, and lastly from *Bhikkhu Bodhi's* [[Personal/Reading/Books/Read/The Noble Eightfold Path by Bhikkhu Bodhi\|The Noble Eightfold Path by Bhikkhu Bodhi]]. I enjoyed *Nagarjuna's* [[Personal/Reading/Books/Read/The Fundamental Wisdom of the Middle Way\|dialectics]] a lot.

In the meantime, in a post on Reddit, I said reincarnation is a minor detail and Buddhism is a philosophy. Naturally, but of course to my amazement then, it triggered quite a few people. 😅

This incident, of course, came with its' epiphany. I've found that thousands of years of practice have made Buddhism as much a religion as, say, Islam is.

So, I read the last book I've mentioned in the vein of knowing more about what Buddhists hold as Buddhism.

## Agreements
My primary curiosity about Buddhism has been sparked by Buddha's alleged philosophical nature, **emphasis on discourse instead of dogma** and eerie accuracy in many matters. By far, I agree with Buddha in:
  1. Four Noble Truths
  2. Impermanence
  3. [[Personal/Reading/Notes and Highlights/The Fundamental Wisdom of the Middle Way#Time：2020-11-21 15:39\|Anātman]]
  4. Dependant Existence
I agree with his idea of mental training to acquire a state of tranquillity to live a life devoid of suffering.

These are all of his original teachings.

## Disagreements
My points of disagreement are also largely Buddha's teaching, however, rarely original. The most important of these is regarding **reincarnation**. Though Buddha categorically denounced Atma or soul, he hasn't denied reincarnation. He adopted it from Hinduism and made it (along with Karma) a core part of Buddhism.

My approach to reconciliation with this embarrassment was to depend on the discursive practice of Buddhism and denouncement of dogmas and the very high emphasis on the necessity of knowing reality as it is. However, Buddhists in Reddit didn't think so. Indeed, they were pretty hostile!
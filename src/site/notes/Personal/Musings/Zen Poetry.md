---
{"title":"Zen Poetry","updated":"2023-01-03T12:46:43+06:00","tags":["poetry","literature","buddhism","zen"],"created":"2021-09-02T16:50:00+06:00","dg-publish":true,"permalink":"/personal/musings/zen-poetry/","dgPassFrontmatter":true}
---

Art essentially is a matter of attachment. An attachment that connects one to the very depth of the world.[^1] Zen, on the other hand, is a practice of detachment. When they met in the atmosphere of Asian aesthetics they have created a genre with blends of various flavours. Exploring Zen poetry is like taking a journey through your existential crisis.

It can be deduced naturally that the poets of zen (often priests) walked on a thin line. Their attachment was not of the mundane kind that we know. Artists, in general, are subtly attached to reality with a kind of super-sensibility, a refined type of attachment.

Precisely for the practice of Zen where the goal is to get enlightenment with a true description of reality and cultured detachment, zen poets scrutinised their attachment within the framework of Buddhism. This scrutiny yielded surprising results in terms of art.

When Ikkyū Sōjun, the eccentric priest, poet, and lover writes:


<div class="transclusion internal-embed is-loaded"><div class="markdown-embed">



LIKE vanishing dew,
a passing apparition
or the sudden flash
of lightning—already gone—
thus should one regard one’s self. 

</div></div>


or


<div class="transclusion internal-embed is-loaded"><div class="markdown-embed">



AND what is mind
and how is it recognized?
It is clearly drawn
in sumi ink, the sound
of breezes drifting through pine. 

</div></div>


He is writing about Zen. These Tanka poems are as much koans as they are poems. A central idea of Buddhism is [[Personal/Reading/Notes and Highlights/The Fundamental Wisdom of the Middle Way#Time：2020-11-21 15:39\|anātman]], which is the denial of the existence of self or soul as an indivisible and indestructible unit. Instead, the self we know is a continuous process working through our senses. It is very much material and the self is considered as an illusion.

Therefore, the self, understood through Zen is superbly temporal like a drop of dew or a flash of lightning. It is as elusive as sound painted in ink.

The teaching of Zen, ironically, can produce the opposite of detachment. For example, Saigyō writes:


<div class="transclusion internal-embed is-loaded"><div class="markdown-embed">



I’D like to divide
myself in order to see,
among these mountains,
each and every flower
of every cherry tree 

</div></div>


Now, this is a very special kind of urge to be attached. It is also a kind of self-denial but an acceptance of nature and the overwhelming ecstasy it holds for us. This attitude, this detachment through attachment, is a recurring theme for many poems. An anonymous Japanese poet writes:


<div class="transclusion internal-embed is-loaded"><div class="markdown-embed">



TO learn how to die,
watch cherry blossoms, observe
chrysanthemums. 

</div></div>


Poetry like this requires a certain emotional quality known as '[[Entities/Concept/Mono no aware\|Mono no aware]]' which is often translated as 'Love for Impermanence' or 'Patheos of Things'. Impermanence is a core idea of Buddhism. This supplied zen poets with a framework to hone their Mono No Aware.

Then comes the anguish. Even with a solid understanding of Zen philosophy poets often faced situations that caused them pain beyond reason. For Bashō, it was when his older brother presented him a lock of white hair of their late mother:


<div class="transclusion internal-embed is-loaded"><div class="markdown-embed">



If I took it in hand,  
it would melt in my hot tears—  
heavy autumn frost

</div></div>


Or, for Kobayashi Issa, it was the death of his baby girl and the crying of his wife. Zen was there, was the impermanence too, so was the anguish:


<div class="transclusion internal-embed is-loaded"><div class="markdown-embed">



This world of dew
is only the world of dew—
and yet … and yet … 

</div></div>


#### References
  1. [[Personal/Reading/Books/Read/The Poetry of Zen by Sam Hamill\|The Poetry of Zen by Sam Hamill, J.P. Seaton]]
  2. [[Personal/Reading/Books/Read/Narrow Road to the Interior_ And Other Writings by Matsuo Bashō\|Narrow Road to the Interior and Other Writings by Matsuo Bashō]]

[^1]: [[Personal/Musings/শিল্পভাবনা ১ হৃদয়বৃত্তি#^715f40\|শিল্পভাবনা ১ হৃদয়বৃত্তি]]
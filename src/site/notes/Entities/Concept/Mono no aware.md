---
{"title":"Mono no aware","aliases":["pathos of things"],"type":"Concept","tags":["concept/aesthetics","concept/literature","concept/art"],"created":"2022-12-28T11:32:51+06:00","updated":"2023-01-02T23:05:52+06:00","dg-publish":true,"permalink":"/entities/concept/mono-no-aware/","dgPassFrontmatter":true}
---

**[Mono no aware](https://en.wikipedia.org/wiki/Mono%20no%20aware)** or the Pathos of Things is probably one of the timeless concepts of the aesthetics that pervades most if not all the forms of art and literatures of every culture.
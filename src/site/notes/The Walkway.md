---
{"title":"The Walkway","dg-home":true,"dg-publish":true,"created":"2023-01-02T21:30:15+06:00","updated":"2023-01-03T01:58:20+06:00","metatags":{"description":"Utsob's Digital Garden","og:description":"Utsob's Digital Garden"},"permalink":"/the-walkway/","tags":"gardenEntry","dgPassFrontmatter":true}
---

তপোবন (Topobon) can be loosly translated to forest-hermitage. In indian myths there are numerous mentions of topobons.

This is my Topobon, my [digital garden](https://cagrimmett.com/notes/2020/11/08/what-are-digital-gardens/), my hermitage (or probably not).

Thoughts here are like every thought, ever-changing.